"""
Test util.py
"""

from bluepill import util
from pathlib import Path
from aiohttp import web


class TestClass(object):
    """
    Test class
    """

    def test_make_directory(self, tmpdir):
        base_path = Path(tmpdir)
        dir_path = base_path.joinpath("test")
        assert util.make_directory(dir_path)
        assert util.make_directory(dir_path / "anotherdir")
        assert dir_path.exists()
        assert dir_path.is_dir()

    def test_make_wrong_directory(self):
        base_path = Path("blablubb")  # root should not work
        dir_path = base_path.joinpath("test")
        assert not util.make_directory(dir_path)
        assert not dir_path.is_dir()

    def test_make_symlink(self, tmpdir):
        base_path = Path(tmpdir)
        p1 = base_path.joinpath("test1")
        p2 = base_path.joinpath("test2")

        assert util.make_directory(p1)
        assert util.make_symlink(p1, p2)
        assert p2.is_symlink()

    def test_make_symlink_failed(self, tmpdir):
        base_path = Path(tmpdir)
        base_path2 = Path("blablubb")
        p1 = base_path.joinpath("test1")
        p2 = base_path2.joinpath("test2")

        assert util.make_directory(p1)
        assert not util.make_symlink(p1, p2)
        assert not p2.is_symlink()

    async def downloader(self, request):
        return web.Response(body=b"testdata")

    def create_downloader(self, loop):
        app = web.Application(loop=loop)
        app.router.add_route("GET", "/testdata", self.downloader)
        return app

    async def test_dl_from_url(self, loop, tmpdir, aiohttp_server):
        server = await aiohttp_server(self.create_downloader(loop=loop))

        path = Path(tmpdir).joinpath("testfile")
        assert await util.dl_from_url(server.make_url("/testdata"), path)
        assert path.is_file()
        with open(path, "rb") as f:
            payload = f.read()

        assert payload == b"testdata"

        assert not await util.dl_from_url(server.make_url("/testdata2"), path)

    def test_delete_file(self, tmpdir):
        p = Path(tmpdir)
        q = p.joinpath("test")
        q.touch()
        assert q.exists()
        util.delete_file(q)
        assert not q.exists()
