"""
Bluepill client functionality
"""

import pyotherside
import sys
import time
import json

# import re

sys.path.append('../')
sys.path.append("/usr/share/harbour-bluepill/lib/python3.8/site-packages")


class BluepillClient:
    """
    Bluepill Client
    """

    def __init__(self, loop=None):
        """
        Initialization of the client

        loop (loop): use an alternative loop
        """

        self._data = []

    def init(self, cache_path: str, local_path: str):
        """
        Initialization of paths.

        store_path (str): Path to store key database
        """

        pyotherside.send("init: ", cache_path, local_path)

    def connect(self) -> None:
        """
        Connect to the matrix server.
        - If no token exists, signal needLogin
        - If token exists startup event loop and signal initReady
        """

        pyotherside.send("needLogin")
        return

    def login(
        self, user: str, password: str, homeserver: str, device_name: str = ""
    ) -> None:
        """
        Login to the server. Will initialize token
        Args:
            user (str): the user name to be logged in
            password (str): the password to login with
            device_name(str): A display name to assign to a newly-created device.
            homeserver (str): the Matrix homeserver
        """

        if user != "testuser" or password != "testpass":
            pyotherside.send("loginError", "Login failed.")
            return

        pyotherside.send("log: login", user, password, homeserver, device_name)
        pyotherside.send("loggedIn", "@sender:testserv.er")
        time.sleep(8)
        pyotherside.send("initReady", '@sender:testserv.er', 'BQPOYPHCNT')

    def get_room_list(self):
        """
        RoomList
        """

        rl = json.load(
            open("/usr/share/harbour-bluepill/python/roomlist.json", "r")
        )
        pyotherside.send("roomList", rl)

        time.sleep(10)

        a = {
            'content': {
                'from_device': 'BQPOYPHCNT',
                'method': 'm.selfas.v1',
                'transaction_id': 'afd03f9c-bbdd-4d54-9038-041f195925fc',
                'key_agreement_protocols': ['curve25519-hkdf-sha256', 'curve25519'],
                'hashes': ['sha256'],
                'message_authentication_codes': ['hkdf-hmac-sha256', 'hmac-sha256'],
                'short_authentication_string': ['emoji', 'decimal']
            },
            'type': 'm.key.verification.start',
            'sender': '@sender:testserv.er',
        }

        pyotherside.send(
            'keyVerificationStart',
            a
        )

    def room_leave(self, room_id):
        time.sleep(1)
        pyotherside.send(
            'roomLeaveResponse',
            room_id
        )

    def get_user_profile(self, userid):
        if userid == '@sender:testserv.er':
            pyotherside.send(
                'userProfile',
                {
                    'user_id': userid,
                    'displayname': 'The Testuser',
                    'avatar_url': '',
                    'other_info': {}
                }
            )
        else:
            pyotherside.send(
                'userProfile',
                {
                    'user_id': userid,
                    'displayname': 'Other testuser',
                    'avatar_url': '',
                    'other_info': {}
                }
            )

    def start_key_verification(self, device_id):
        pyotherside.send(
            'StartKeyVerification',
            device_id
        )
        time.sleep(2)

        pyotherside.send(
            'keyVerificationKey',
            '@sender:serv.er',
            '@devsender:serv.er',
            'BQPOYPHCN',
            '40794977-81c6-4ca8-b69c-198cda6ae319',
            [
                ('🎸', 'Guitar'),
                ('✂️', 'Scissors'),
                ('🦄', 'Unicorn'),
                ('🎅', 'Santa'),
                ('☎️', 'Telephone'),
                ('⚽', 'Ball'),
                ('📁', 'Folder')
            ]
        )

    def cancel_key_verification(self, transaction_id):
        pyotherside.send(
            'keyVerificationCanceled',
            transaction_id
        )

    def accept_key_verification(self, transaction_id):
        pyotherside.send(
            "acceptedKeyVerification",
            transaction_id
        )

        time.sleep(2)

        pyotherside.send(
            'keyVerificationKey',
            '@sender:serv.er',
            '@devsender:serv.er',
            'BQPOYPHCN',
            '40794977-81c6-4ca8-b69c-198cda6ae319',
            [
                ('🎸', 'Guitar'),
                ('✂️', 'Scissors'),
                ('🦄', 'Unicorn'),
                ('🎅', 'Santa'),
                ('☎️', 'Telephone'),
                ('⚽', 'Ball'),
                ('📁', 'Folder')
            ]
        )

    def confirm_key_verification(self, transaction_id):
        pyotherside.send(
            'confirmedKeyVerification',
            transaction_id
        )

    def active_user_devices(self, user_id):
        d = [
            {
                'user_id': '@sender:testserv.er',
                'device_id': 'GDKSWDSLFL',
                'display_name': 'the Device nr 1.',
                'trust_state': 'unset',
                'keys': {
                    'ed25519': '12345678901234567890123456789012345678901234',
                    'curve25519': 'fakestring'
                }
            },
            {
                'user_id': '@sender:testserv.er',
                'device_id': 'BQPOYPHCNT',
                'display_name': 'the Device nr 2.',
                'trust_state': 'verified',
                'keys': {
                    'ed25519': '12345678901234567890123456789012345678901234',
                    'curve25519': 'fakestring'
                }
            },
            {
                'user_id': '@sender:testserv.er',
                'device_id': 'FIGKWDJSEL',
                'display_name': 'the Device nr 3.',
                'trust_state': 'unset',
                'keys': {
                    'ed25519': '12345678901234567890123456789012345678901234',
                    'curve25519': 'fakestring'
                }
            },
            {
                'user_id': '@sender:testserv.er',
                'device_id': 'OKGKSJCSLA',
                'display_name': 'the Device nr 3.',
                'trust_state': 'blacklisted',
                'keys': {
                    'ed25519': '12345678901234567890123456789012345678901234',
                    'curve25519': 'fakestring'
                }
            },
            {
                'user_id': '@sender:testserv.er',
                'device_id': 'KEITSHBHLS',
                'display_name': 'the Device nr 4.',
                'trust_state': 'ignored',
                'keys': {
                    'ed25519': '12345678901234567890123456789012345678901234',
                    'curve25519': 'fakestring'
                }
            },
            {
                'user_id': '@sender:testserv.er',
                'device_id': 'ECHJAKJFEK',
                'display_name': 'the Device nr 5.',
                'trust_state': 'unset',
                'keys': {
                    'ed25519': '12345678901234567890123456789012345678901234',
                    'curve25519': 'fakestring'
                }
            },
            {
                'user_id': '@sender:testserv.er',
                'device_id': 'UFKJRHVNDE',
                'display_name': 'the Device nr 6.',
                'trust_state': 'unset',
                'keys': {
                    'ed25519': '12345678901234567890123456789012345678901234',
                    'curve25519': 'fakestring'
                }
            },
            {
                'user_id': '@sender:testserv.er',
                'device_id': 'IEHJSKCHEK',
                'display_name': 'the Device nr 7.',
                'trust_state': 'verified',
                'keys': {
                    'ed25519': '12345678901234567890123456789012345678901234',
                    'curve25519': 'fakestring'
                }
            },
            {
                'user_id': '@sender:testserv.er',
                'device_id': 'WSKFHSLHXX',
                'display_name': 'the Device nr 8.',
                'trust_state': 'unset',
                'keys': {
                    'ed25519': '12345678901234567890123456789012345678901234',
                    'curve25519': 'fakestring'
                }
            },
            {
                'user_id': '@sender:testserv.er',
                'device_id': 'IFHCLESSOR',
                'display_name': 'the Device nr 9.',
                'trust_state': 'verified',
                'keys': {
                    'ed25519': '12345678901234567890123456789012345678901234',
                    'curve25519': 'fakestring'
                }
            },
            {
                'user_id': '@sender:testserv.er',
                'device_id': 'LGKHEKXNVV',
                'display_name': 'the Device nr 10.',
                'trust_state': 'ignored',
                'keys': {
                    'ed25519': '12345678901234567890123456789012345678901234',
                    'curve25519': 'fakestring'
                }
            },
            {
                'user_id': '@sender:testserv.er',
                'device_id': 'OOOSFERJDS',
                'display_name': 'the Device nr 11.',
                'trust_state': 'unset',
                'keys': {
                    'ed25519': '12345678901234567890123456789012345678901234',
                    'curve25519': 'fakestring'
                }
            },
            {
                'user_id': '@sender:testserv.er',
                'device_id': 'GKDDKDETTE',
                'display_name': 'the Device nr 12.',
                'trust_state': 'verified',
                'keys': {
                    'ed25519': '12345678901234567890123456789012345678901234',
                    'curve25519': 'fakestring'
                }
            },
            {
                'user_id': '@sender:testserv.er',
                'device_id': 'KKDERJWXEE',
                'display_name': 'the Device nr 13.',
                'trust_state': 'verified',
                'keys': {
                    'ed25519': '12345678901234567890123456789012345678901234',
                    'curve25519': 'fakestring'
                }
            }
        ]
        pyotherside.send('activeUserDevices', d)

    def verify_device(self, device_id, user_id):
        pyotherside.send("deviceVerified", device_id)

    def delete_devices(self, devices, user, password):
        if password != "testpass":
            pyotherside.send('devicesDeletionAuthFailed', devices)
        else:
            pyotherside.send('devicesDeleted', devices)

    def get_bucket(self, room_id, bucket_id):
        bucket = json.load(
            open("/usr/share/harbour-bluepill/python/bucket1.json", "r")
        )
        pyotherside.send("bucketGet", room_id, bucket_id, bucket)

    def get_last_bucket_from(self, room_id, bucket_id):
        bucket = json.load(
            open("/usr/share/harbour-bluepill/python/bucket2.json", "r")
        )
        pyotherside.send("bucketGetFrom", room_id, bucket_id, bucket)

    # async def start_event_loop(self):
    #     """
    #     Start the event loop thread
    #     """

    #     self._client.add_event_callback(self.message_text_cb, nio.RoomMessageText)
    #     self._client.add_event_callback(self.message_emote_cb, nio.RoomMessageEmote)
    #     self._client.add_event_callback(self.message_notice_cb, nio.RoomMessageNotice)
    #     self._client.add_event_callback(self.message_image_cb, nio.RoomMessageImage)
    #     self._client.add_event_callback(self.message_audio_cb, nio.RoomMessageAudio)
    #     self._client.add_event_callback(self.message_video_cb, nio.RoomMessageVideo)
    #     self._client.add_event_callback(self.message_file_cb, nio.RoomMessageFile)

    #     self._client.add_ephemeral_callback(
    #         self.ephemeral_typingnotice_cb, nio.TypingNoticeEvent
    #     )

    #     self._client.add_response_callback(
    #         self.response_synced_cb, nio.responses.SyncResponse
    #     )

    #     self._client.add_response_callback(
    #         self.response_sync_error_cb, nio.responses.SyncError
    #     )

    #     await self._client.sync_forever(timeout=30000)

    # async def message_text_cb(self, room, event):
    #     event.server_timestamp = int(event.server_timestamp / 1000)
    #     event.body = emoji.emojize(event.body, use_aliases=True)
    #     event.formatted_body = self._markdown.convert(event.body)
    #     pyotherside.send(
    #         "roomMessageText",
    #         room.room_id,
    #         room.user_name(event.sender),
    #         event.source,
    #         event.verified,
    #         event.decrypted,
    #     )

    # async def message_emote_cb(self, room, event):
    #     event.server_timestamp = int(event.server_timestamp / 1000)
    #     pyotherside.send(
    #         "roomMessageEmote", room.room_id, room.user_name(event.sender), event.source
    #     )

    # async def message_notice_cb(self, room, event):
    #     event.server_timestamp = int(event.server_timestamp / 1000)
    #     pyotherside.send(
    #         "roomMessageNotice",
    #         room.room_id,
    #         room.user_name(event.sender),
    #         event.source,
    #     )

    # async def message_image_cb(self, room, event):
    #     event.server_timestamp = int(event.server_timestamp / 1000)
    #     pyotherside.send(
    #         "roomMessageImage", room.room_id, room.user_name(event.sender), event.source
    #     )

    # async def message_audio_cb(self, room, event):
    #     event.server_timestamp = int(event.server_timestamp / 1000)
    #     pyotherside.send(
    #         "roomMessageAudio", room.room_id, room.user_name(event.sender), event.source
    #     )

    # async def message_video_cb(self, room, event):
    #     event.server_timestamp = int(event.server_timestamp / 1000)
    #     pyotherside.send(
    #         "roomMessageVideo", room.room_id, room.user_name(event.sender), event.source
    #     )

    # async def message_file_cb(self, room, event):
    #     event.server_timestamp = int(event.server_timestamp / 1000)
    #     pyotherside.send(
    #         "roomMessageFile", room.room_id, room.user_name(event.sender), event.source
    #     )

    # async def ephemeral_typingnotice_cb(self, room, event):
    #     #     event.server_timestamp = int(event.server_timestamp / 1000)
    #     pyotherside.send("typingNotice", room.room_id, event.users)

    # async def response_synced_cb(self, response):
    #     # pyotherside.send("syncResponse", response.next_batch)
    #     pass

    # async def response_sync_error_cb(self, response):
    #     pyotherside.send("syncErrorResponse", response.message)

    # def logout(self):
    #     """
    #     Log system out
    #     """

    #     try:
    #         self._loop.run_until_complete(
    #             self._client.logout(self._data.access_token, all_devices=True)
    #         )
    #     except:
    #         pyotherside.send("logoutError")

    #     self._client = None
    #     ClientDataFactory().client_data = None
    #     Memory().delete_object(CLIENTDATA)
    #     self._data = ClientDataFactory().clientData
    #     self._data.access_token = None
    #     self._data.save()
    #     pyotherside.send("loggedOut")

    # def close(self):
    #     """
    #     Close the device: shutdown asyncio loop
    #     """

    #     asyncio.run_coroutine_threadsafe(self._loop.shutdown_asyncgens(), self._loop)

    #     try:
    #         self._loop.stop()
    #         self._loop.close()
    #     except:
    #         pass

    # def room_messages(self, room_id: str, start: str, limit: int = 10):
    #     """
    #     Get room messages
    #     Args:
    #     room_id (str): Id of room
    #     start (str): start position id (or None)
    #     limit (int): limit of events (default 10)
    #     """

    #     return asyncio.run_coroutine_threadsafe(
    #         self._client.room_messages(
    #             room_id, start, "", nio.MessageDirection.back, limit
    #         ),
    #         self._loop,
    #     ).result()

    # def get_devices(self):
    #     """
    #     Get user's devices
    #     """

    #     return asyncio.run_coroutine_threadsafe(
    #         self._client.devices(), self._loop
    #     ).result()

    # def room_update(self, response):
    #     """
    #     Update room information list
    #     """

    #     invite = response.rooms.invite
    #     # joined rooms are interpreted in self._client.rooms
    #     leave = response.rooms.leave

    #     for lroom in leave:
    #         # delete all rooms in leave from room list
    #         pass

    #     for room in self._client.rooms:
    #         # now set actual data to joined rooms
    #         # rd = RoomDataFactory().room_data(room.room_id)
    #         pass

    #     for iroom in invite:
    #         # we need to send messages for invited rooms
    #         pass

    # def sync(self):
    #     """
    #     Sync process
    #     """

    #     try:
    #         resp = self._loop.run_until_complete(
    #             self._client.sync(
    #                 since=self._data.next_batch,
    #                 full_state=self._data.full_state,
    #             )
    #         )
    #     except SyncError:
    #         pass

    #     self.room_update(resp)

    # def set_listeners(self):
    #     """
    #     set listeners for event loop
    #     """

    # Startup Event loop

    # # ----------------------------------------- oldstuff

    # # room_update(response)
    # # event_update()
    # # set_listeners()

    # pyotherside.send("isConnected")

    # pyotherside.send("login")
    # try:
    #     self._client = MatrixClient(homeserver)
    #     self._data.token = self._client.login(
    #         user, password, limit=100, sync=True
    #     )
    #     pyotherside.send("gotToken", self._data.token)
    # except MatrixRequestError:
    #     pyotherside.send("loginFailed")
    #     return

    # pyotherside.send("loggedIn")

    # self._data.encryption = True
    # user = self._client.get_user(self._data.user_id)
    # self._data.user_name = user.get_display_name()
    # avatar_url = user.get_avatar_url()
    # self._data.user_avatar_url = (
    #     self._get_url(avatar_url) if avatar_url else ""
    # )

    # response = self._client.api.sync()

    # self.get_room_dir(response)
    # self.get_rooms()
    # self._data.save()

    # self.start_room_event_listener()
    # self.start_listener()

    # pyotherside.send("initReady", self._data.user_id)
    # pyotherside.send("log", "token", self._data.token)

    # pyotherside.send("initReady", self._data.user_id)

    # Startup Event loop

    # # room_update(response)
    # # event_update()
    # # set_listeners()

    # pyotherside.send("isConnected")

    # ############################# old

    # pyotherside.send("log", "token", self._data.token)
    # try:
    #     self._client = MatrixClient(
    #         self._data.homeserver,
    #         user_id=self._data.user_id,
    #         token=self._data.token,
    #     )
    # except:
    #     pyotherside.send("connectFailed")
    #     self._client = None
    #     pyotherside.send("initReady", self._data.user_id)
    #     return

    # pyotherside.send("isConnected")

    # self.sync()
    # self.sync_rooms()
    # self.start_room_event_listener()
    # self.start_listener()

    # def do_exit(self):
    #     """
    #     Stop the listener Threads
    #     """

    #     self.stop_listener()
    #     self.stop_room_event_listener()
    #     self.save()

    # def join_room(self, room_id_or_alias: str) -> bool:
    #     """
    #     Join the room
    #     """

    #     try:
    #         room = self._client.join_room(room_id_or_alias)
    #     except MatrixRequestError as e:
    #         pyotherside.send("joinRoomFailed", e.content)
    #         return False
    #     if not room:
    #         return False

    #     self._create_room(room_id=None, rd=room)
    #     self._data.roomdir.append(room.room_id)
    #     self._data.save()
    #     return True

    # def get_room_dir(self, response):
    #     """
    #     Get direct rooms from account_data
    #     """

    #     account_data = response["account_data"]
    #     event = next(
    #         filter(lambda x: x["type"] == "m.direct", account_data["events"]),
    #         None,
    #     )
    #     if event:
    #         pyotherside.send("log", "m.direct in account_data")
    #         self._data.roomdir = [
    #             j for i in event["content"].values() for j in i
    #         ]

    # def format_event(self, room_id: str, event):
    #     """
    #     TODO: event.py?
    #     """

    #     image = ""
    #     user_id = event["sender"]
    #     user = self._client.get_user(user_id)
    #     try:
    #         avatar_url = user.get_avatar_url()
    #     except:
    #         avatar_url = ""
    #     avatar_url = self._get_url(avatar_url) if avatar_url else ""
    #     dat = event["origin_server_ts"] / 1000
    #     event["origin_server_ts"] = dat
    #     try:
    #         name = user.get_friendly_name()
    #     except:
    #         name = ""
    #     try:
    #         if event["content"]["msgtype"] == "m.image":
    #             image = event["content"]["url"]
    #             pyotherside.send("image: ", image)
    #             image = (
    #                 self._get_url(self._client.api.get_download_url(image))
    #                 if image
    #                 else ""
    #             )
    #     except:
    #         image = ""

    #     try:
    #         if event["type"] == "m.sticker":
    #             image = event["content"]["url"]
    #             if image:
    #                 image = self._get_url(
    #                     self._client.api.get_download_url(image)
    #                 )
    #     except:
    #         image = ""

    #     try:
    #         if event["content"]["msgtype"] == "m.text":
    #             event["content"]["body"] = emoji.emojize(
    #                 event["content"]["body"], use_aliases=True
    #             )
    #             event["content"]["formatted_body"] = self._markdown.convert(
    #                 event["content"]["body"]
    #             )
    #             event["content"]["format"] = "org.matrix.custom.html"
    #     except:
    #         pass
    #     room_data = RoomDataFactory().room_data(room_id)
    #     if room_data:
    #         room_name = room_data.display_name
    #     else:
    #         room_name = ""

    #     asect = util.format_full_date(dat)
    #     return (
    #         dat,
    #         {
    #             "event": event,
    #             "name": name,
    #             "rname": room_name,
    #             "user_id": user_id,
    #             "edate": dat,
    #             "avatar_url": avatar_url,
    #             "image": image,
    #             "asect": asect,
    #         },
    #     )

    # def room_append_event(
    #     self, room_id: Optional[str], event, save: bool = True
    # ):
    #     """
    #     Append a room event
    #     TODO: to room.py
    #     """

    #     if not room_id:
    #         return None
    #     room_data = RoomDataFactory().room_data(room_id)
    #     if not room_data:
    #         return None
    #     room_name = room_data.display_name
    #     room_level = room_data.level

    #     image = ""
    #     user_id = event["sender"]
    #     user = self._client.get_user(user_id)
    #     try:
    #         avatar_url = user.get_avatar_url()
    #     except:
    #         avatar_url = ""
    #     avatar_url = self._get_url(avatar_url) if avatar_url else ""
    #     dat = event["origin_server_ts"] / 1000
    #     event["origin_server_ts"] = dat
    #     try:
    #         name = user.get_friendly_name()
    #     except:
    #         name = ""
    #     try:
    #         if event["content"]["msgtype"] == "m.image":
    #             image = event["content"]["url"]
    #             pyotherside.send("image: ", image)
    #             image = (
    #                 self._get_url(self._client.api.get_download_url(image))
    #                 if image
    #                 else ""
    #             )
    #     except:
    #         image = ""

    #     try:
    #         if event["type"] == "m.sticker":
    #             image = event["content"]["url"]
    #             if image:
    #                 image = self._get_url(
    #                     self._client.api.get_download_url(image)
    #                 )
    #     except:
    #         image = ""

    #     try:
    #         if event["content"]["msgtype"] == "m.text":
    #             event["content"]["body"] = emoji.emojize(
    #                 event["content"]["body"], use_aliases=True
    #             )
    #             event["content"]["formatted_body"] = self._markdown.convert(
    #                 event["content"]["body"]
    #             )
    #             event["content"]["format"] = "org.matrix.custom.html"
    #     except:
    #         pass

    #     asect = util.format_full_date(dat)

    #     event_data = {
    #         "event": event,
    #         "name": name,
    #         "rname": room_name,
    #         "rlevel": room_level,
    #         "user_id": user_id,
    #         "edate": dat,
    #         "avatar_url": avatar_url,
    #         "image": image,
    #         "asect": asect,
    #     }
    #     room_data.events.append(event_data)
    #     room_data.last_time = dat
    #     if save:
    #         room_data.end_token = self._client.api.get_room_messages(
    #             room_id, None, "f"
    #         )["end"]
    #         room_data.save()
    #     return event_data

    # def _create_room(self, room_id: str = None, rd=None):
    #     """
    #     Create a room_id
    #     """

    #     if rd and not room_id:
    #         room_id = rd.room_id
    #     else:
    #         rd = self._client.rooms[room_id]

    #     room_data = RoomDataFactory().room_data(room_id)
    #     room_data.room_id = room_id
    #     room_data.name = rd.name
    #     pyotherside.send("syncingRooms", rd.display_name)
    #     room_data.display_name = rd.display_name
    #     room_data.topic = rd.topic
    #     tags = rd.get_tags()["tags"]
    #     for tag in tags:
    #         if tag == "m.favourite":
    #             room_data.level = "fav"
    #             room_data.lorder = tags[tag].get("order", 0)
    #         elif tag == "m.lowpriority":
    #             room_data.level = "low"
    #         else:
    #             room_data.level = "normal"

    #     events = rd.get_events()
    #     room_data.events = []
    #     for event in events:
    #         self.room_append_event(room_id, event, save=False)

    #     if room_id in self._data.roomdir:
    #         for member in rd.get_joined_members():
    #             if member.user_id != self._data.user_id:
    #                 try:
    #                     avatar_url = member.get_avatar_url()
    #                     user_id = member.user_id
    #                     if avatar_url:
    #                         room_data.avatar_url = self._get_url(avatar_url)
    #                 except:
    #                     avatar_url = ""
    #                     user_id = ""
    #     else:
    #         for e in self._client.api.get_room_state(room_id):
    #             if e["type"] == "m.room.avatar":
    #                 aurl = e["content"]["url"]
    #                 if aurl:
    #                     avatar_url = self._client.api.get_download_url(aurl)
    #                     room_data.avatar_url = self._get_url(avatar_url)
    #         user_id = room_id

    #     room_data.user_id = user_id
    #     room_data.encrypted = rd.encrypted
    #     room_data.end_token = self._client.api.get_room_messages(
    #         room_id, None, "f"
    #     )["end"]
    #     room_data.start_token = self._client.api.get_room_messages(
    #         room_id, None, "f"
    #     )["start"]
    #     room_data.members = self.get_room_members(room_id)

    #     room_data.save()

    # def _sync_room(self, room_id: str):
    #     """
    #     Sync room data
    #     """

    #     rd = self._client.rooms[room_id]
    #     room_data = RoomDataFactory().room_data(room_id)
    #     if not room_data.room_id:
    #         self._create_room(room_id)
    #         return

    #     room_data.name = rd.name
    #     room_data.display_name = rd.display_name
    #     room_data.topic = rd.topic
    #     tags = rd.get_tags()["tags"]
    #     for tag in tags:
    #         if tag == "m.favourite":
    #             room_data.level = "fav"
    #             room_data.lorder = tags[tag]["order"]
    #         elif tag == "m.lowpriority":
    #             room_data.level = "low"
    #         else:
    #             room_data.level = "normal"
    #     events = self._client.api.get_room_messages(
    #         room_id, room_data.end_token, "f"
    #     )["chunk"]
    #     for event in events:
    #         pyotherside.send("newEvent")
    #         self.room_append_event(room_id, event, save=False)

    #     room_data.end_token = self._client.api.get_room_messages(
    #         room_id, None, "f"
    #     )["end"]
    #     room_data.save()

    # def sync_rooms(self):
    #     """
    #     Synchronize rooms from last time
    #     """

    #     self._data.rooms = []
    #     for room in self._client.rooms:
    #         self._sync_room(room)
    #         self._data.rooms.append(room)

    #     pyotherside.send("initReady", self._data.user_id)

    # def get_rooms(self):
    #     """
    #     Initial room fetching and preparing room_data objects
    #     """

    #     for room in self._client.rooms:
    #         self._create_room(room)
    #         self._data.rooms.append(room)

    # def get_roomlist(self):
    #     """
    #     Get roomlisst
    #     """

    #     for room in self._data.rooms:
    #         rd = Memory().get_object(room)
    #         pyotherside.send("log", "outputting", rd.room_id)
    #         if rd:
    #             levent = rd.events[len(rd.events) - 1] if rd.events else None
    #             yield {
    #                 "room_id": rd.room_id,
    #                 "user_id": rd.user_id,
    #                 "room_name": rd.display_name,
    #                 "prio": rd.level if rd.level else "",
    #                 "direct": room in self._data.roomdir,
    #                 "avatar_url": rd.avatar_url,
    #                 "time": rd.last_time,
    #                 "encrypted": rd.encrypted,
    #                 "last_event": levent,
    #             }

    # def get_room(self, room_id: str):
    #     """
    #     get single room data
    #     """
    #     rd = Memory().get_object(room_id)
    #     if rd:
    #         return {
    #             "room_id": rd.room_id,
    #             "user_id": rd.user_id,
    #             "room_name": rd.display_name,
    #             "prio": rd.level,
    #             "direct": room_id in self._data.roomdir,
    #             "avatar_url": rd.avatar_url,
    #             "time": rd.last_time,
    #             "encrypted": rd.encrypted,
    #         }

    #     return None

    # def listener_exception(self, e):
    #     """
    #     Listener exception
    #     """

    #     pyotherside.send("Listener exception")
    #     # pyotherside.send("restartListener")

    # def seen_message(self, room_id: str):
    #     """
    #     Send message receipt
    #     """

    #     rd = RoomDataFactory().room_data(room_id)

    #     if not rd:
    #         return

    #     levent = rd.events[len(rd.events) - 1]["event"]

    #     try:
    #         lr = rd.last_read
    #     except:
    #         lr = 0
    #     if lr < levent["origin_server_ts"]:
    #         pyotherside.send("seen_message")
    #         rd.last_read = levent["origin_server_ts"]
    #         self._client.api.send_read_markers(
    #             room_id, levent["event_id"], levent["event_id"]
    #         )
    #         rd.save()

    # def is_typing(self, room_id: str, timeout: int = 30000):
    #     """
    #     Send typing
    #     Args:
    #         room_id(str): The room ID.
    #         timeout: Timeout of typing
    #     """

    #     content = {"typing": True, "timeout": timeout}
    #     path = "/rooms/{}/typing/{}".format(
    #         quote(room_id), quote(self._data.user_id)
    #     )
    #     self._client.api._send("PUT", path, content)

    # def start_listener(self):
    #     """
    #     start listener_thread
    #     """

    #     try:
    #         self._client.start_listener_thread(
    #             timeout_ms=60000, exception_handler=self.listener_exception
    #         )
    #     except:
    #         pyotherside.send("Starting listener thread failed")
    #         pyotherside.send("restartListener")

    # def restart_listener(self):
    #     """
    #     Restart the listener thread
    #     """

    #     try:
    #         self.stop_listener()
    #     except:
    #         pyotherside.send("log", "restartListener: cannot stop listener")
    #     try:
    #         self.start_listener()
    #     except:
    #         pyotherside.send("log", "restartListener: cannot start listener")

    # def stop_listener(self):
    #     """
    #     stop the listener_thread
    #     """

    #     try:
    #         self.client._stop_listener_thread()
    #     except:
    #         pyotherside.send("Stopping listener thread failed")

    # def room_listener(self, event):
    #     """
    #     The room listener
    #     """

    #     room_id = event["room_id"]
    #     rd = Memory().get_object(room_id)
    #     if event["type"] == "m.room.redaction":
    #         count = 0
    #         for ev in rd.events:
    #             if ev["event"]["event_id"] == event["redacts"]:
    #                 # event["event_id"] = event["redacts"]
    #                 dat, rd.events[count] = self.format_event(room_id, event)
    #                 rd.last_event = dat
    #                 rd.save()
    #                 pyotherside.send("mRedactEvent", room_id, event)
    #                 return
    #             count += 1

    #     event_data = self.room_append_event(room_id, event)
    #     if event_data["user_id"] == self._data.user_id:
    #         rd.set_revent(event_data["event"]["event_id"])

    #     pyotherside.send("mRoomEvent", room_id, event_data)
    #     pyotherside.send("roomUnread", event["room_id"], rd.unread_count)

    # def receipt_listener(self, event):
    #     """
    #     A user has read some event
    #     """

    #     for receipt in event["content"]:
    #         for uid in event["content"][receipt]["m.read"]:
    #             if uid == self._data.user_id:
    #                 rd = Memory().get_object(event["room_id"])
    #                 rd.set_revent(receipt)
    #                 pyotherside.send(
    #                     "roomUnread", event["room_id"], rd.unread_count
    #                 )

    # def get_unread_count(self, room_id: str):
    #     rd = Memory().get_object(room_id)

    #     if rd:
    #         pyotherside.send("roomUnread", room_id, rd.unread_count)

    # def typing_listener(self, event):
    #     """
    #     A user is typing (or not)
    #     """

    #     if len(event["content"]["user_ids"]) == 0:
    #         pyotherside.send("stopTyping", event["room_id"])
    #         return

    #     users = []

    #     for user_id in event["content"]["user_ids"]:
    #         user = self._client.get_user(user_id)
    #         user_name = user.get_display_name()
    #         avatar_url = user.get_avatar_url()
    #         if not avatar_url:
    #             image = ""
    #         else:
    #             image = self._get_url(avatar_url)

    #         users.append(
    #             {"user_id": user_id, "user_name": user_name, "image": image}
    #         )
    #     pyotherside.send("startTyping", event["room_id"], users)

    # def stop_room_event_listener(self):
    #     """
    #     Leave listener
    #     """

    #     self._client.remove_listener(self._m_room_message_listener)
    #     self._client.remove_listener(self._m_room_sticker_listener)
    #     self._client.remove_listener(self._m_redaction_listener)
    #     self._client.remove_ephemeral_listener(self._m_typing_listener)
    #     self._client.remove_ephemeral_listener(self._m.receipt_listener)
    #     self._m_room_message_listener = None
    #     self._m_room_sticker_listener = None
    #     self._m.redaction_listener = None
    #     self._m_typing_listener = None
    #     self._m_receipt_listener = None

    # def start_room_event_listener(self):
    #     """
    #     set room listener
    #     """

    #     if self._m_room_message_listener:
    #         self._client.remove_listener(self._m_room_message_listener)
    #     if self._m_room_sticker_listener:
    #         self._client.remove_listener(self._m_room_sticker_listener)
    #     if self._m_redaction_listener:
    #         self._client.remove_listener(self._m_redaction_listener)
    #     self._m_room_message_listener = self._client.add_listener(
    #         self.room_listener, event_type="m.room.message"
    #     )
    #     self._m_room_sticker_listener = self._client.add_listener(
    #         self.room_listener, event_type="m.sticker"
    #     )
    #     self._m_redaction_listener = self._client.add_listener(
    #         self.room_listener, event_type="m.room.redaction"
    #     )
    #     self._m_typing_listener = self._client.add_ephemeral_listener(
    #         self.typing_listener, event_type="m.typing"
    #     )
    #     self._m_receipt_listener = self._client.add_ephemeral_listener(
    #         self.receipt_listener, event_type="m.receipt"
    #     )

    # def get_room_members(self, room_id: Optional[str]):
    #     """
    #     get members and event list for room_id
    #     """

    #     members = []
    #     for member in self._client.api.get_room_members(room_id)["chunk"]:
    #         try:
    #             if member["content"]["avatar_url"]:
    #                 image = self._get_url(member["content"]["avatar_url"])
    #             else:
    #                 image = ""
    #         except:
    #             image = ""

    #         try:
    #             member["origin_server_ts"] /= 1000
    #             members.append(
    #                 {
    #                     "event_id": member["event_id"],
    #                     "name": member["content"]["displayname"],
    #                     "avatar_url": image,
    #                     "time": member["origin_server_ts"],
    #                 }
    #             )
    #         except:
    #             pass

    #     return members

    # def get_room_events(self, room_id: str):
    #     """
    #     get room events for room_id
    #     """

    #     rd = Memory().get_object(room_id)
    #     events = rd.get_room_events()
    #     members = rd.members
    #     pyotherside.send("roomEvents", events, members)

    # def room_member_list(self, room_id: str):
    #     """
    #     Send room member list
    #     """
    #     rd = Memory().get_object(room_id)
    #     pyotherside.send("roomMembers", rd.members)

    # def get_user_info(self):
    #     pyotherside.send(
    #         "userInfo",
    #         {
    #             "lname": self._data.user_name,
    #             "user_id": self._data.user_id,
    #             "avatar_url": self._data.user_avatar_url,
    #         },
    #     )

    # def _get_url(self, url: str):
    #     """
    #     """

    #     if url == "":
    #         return url

    #     f = urlparse(url).path
    #     filename = f[f.rfind("/") + 1 :]
    #     path = os.path.join(Memory().cache_home, filename)
    #     if not os.path.exists(path):
    #         pyotherside.send("get_url: need to download", path)
    #         try:
    #             util.dl_from_url(url, path)
    #         except:
    #             return ""

    #     return path

    # # def sync(self, first: bool = False):
    # #     """
    # #     Sync with server
    # #     """

    # #     pyotherside.send("Client data syncer")
    # #     try:
    # #         syncdata = self._client.api.sync(
    # #             since=self._ts_token, full_state=first
    # #         )
    # #     except:
    # #         syncdata = None

    # #     if not syncdata:
    # #         pyotherside.send("Matrix syncing failed")
    # #         return

    # #     event = next(
    # #         filter(
    # #             lambda x: x["type"] == "com.gitlab.cy8aer.bluepill",
    # #             None
    # #             # account_data["events"],
    # #         ),
    # #         None,
    # #     )
    # #     if event:
    # #         pyotherside.send(
    # #             "log", "com.gitlab.cy8aer.bluepill in account_data"
    # #         )
    # #         pass

    # #     for room in syncdata["rooms"]["join"]:
    # #         # configuration of rooms
    # #         pass

    # #     for room in syncdata["rooms"]["invite"]:
    # #         # you are invited to new rooms
    # #         pass

    # #     for room in syncdata["rooms"]["leave"]:
    # #         # you left rooms or you have been banned from a room
    # #         pass

    # #     for presence in syncdata["presence"]["events"]:
    # #         # presence of users
    # #         # content:
    # #         #     currently_active
    # #         #     last_active_ago
    # #         #     presence
    # #         # sender
    # #         # type
    # #         pass

    # #     # device_lists

    # #     self.get_room_dir(syncdata)

    # #     # device_time_keys_count

    # def save(self):
    #     """
    #     Save myself
    #     """

    #     # Memory().save_object(STORE_NAME, self)

    # # Data providers

    # def send_message(self, room_id: str, message: str):
    #     """
    #     Markdown transform a message and send it
    #     """

    #     message = emoji.emojize(message, use_aliases=True)
    #     pyotherside.send("send_message", room_id, message)
    #     htmlmsg = self._markdown.convert(message)
    #     room = self._client.rooms[room_id]
    #     pyotherside.send("sending message ", htmlmsg, message)
    #     room.send_html(htmlmsg, body=message)

    # def send_image(self, room_id: str, image_file: str):
    #     """
    #     upload an image and send a message
    #     """

    #     mimes = {
    #         "jpg": "image/jpeg",
    #         "jpeg": "image/jpeg",
    #         "gif": "image/gif",
    #         "png": "image/png",
    #     }

    #     ext = os.path.splitext(image_file)[1][1:]
    #     content_type = mimes[ext]

    #     pyotherside.send("log", "uploading image")
    #     try:
    #         with open(image_file, "rb") as image:
    #             mxc = self._client.upload(image.read(), content_type)
    #     except MatrixUnexpectedResponse:
    #         pyotherside.send(
    #             "imageSendFailed", "image upload unexpected response"
    #         )
    #         return
    #     except MatrixRequestError:
    #         pyotherside.send("imageSendFailed", "image upload request error")
    #         return
    #     except:
    #         pyotherside.send("imageSendFailed", "other error")
    #         return

    #     pyotherside.send("log", "send image message")
    #     room = self._client.rooms[room_id]
    #     try:
    #         room.send_image(mxc, os.path.split(image_file)[1])
    #     except:
    #         pyotherside.send("imageSendFailed", "image message failed")

    #     pyotherside.send("imageSent")

    # def redact(self, room_id: str, event_id: str, reason: str = None):
    #     """
    #     Redact a message
    #     """

    #     rd = self._client.rooms[room_id]
    #     rd.redact_message(event_id, reason)

    # @property
    # def user_name(self):
    #     """
    #     Return the user_name
    #     """

    #     return self._user_name

    # @property
    # def user_id(self):
    #     """
    #     Return the user_id
    #     """

    #     return self._user_id


class BluepillClientFactory():
    """
    Factory which creates a BluepillClient if it does not exist
    """

    def __init__(self):
        """
        Initialization
        """

        self._bluepill_client = None

    def get_client(self):
        """
        get the Bluepill Client
        """

        if not self._bluepill_client:
            self._bluepill_client = BluepillClient()

        return self._bluepill_client
