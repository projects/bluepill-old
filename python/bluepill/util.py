"""
some utilities
"""

# import urllib.request
from pathlib import Path
import aiohttp
import aiofiles

from base64 import b64encode, b64decode
from Crypto.Cipher import ChaCha20
from Crypto.Random import get_random_bytes
import hashlib

# import pyotherside

import time
import datetime

user_agent = "PodQast/2.3 +https://gitlab.com/cy8aer/podqast"
user_agent2 = (
    "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"
)


def make_directory(path: Path) -> bool:
    """
    Tries to create a directory if it does not exist already

    Args:
        path (Path): The path of the directory
    Return:
        bool: True if directory exists, False otherwise
    """
    try:
        path.mkdir(mode=0o755, parents=False, exist_ok=True)
        return True
    except:
        return False


def make_symlink(frompath: Path, topath: Path) -> bool:
    """
    Create a symlink

    Args:
        frompath (Path): original file
        topath (Path): the path to link to
    Return:
        bool: False if fails.
    """

    if topath.is_symlink():
        return True

    try:
        topath.symlink_to(frompath)
        return True
    except:
        return False


# async def dl_from_url(url: str, path: Path) -> bool:
async def dl_from_url(url: str, path: str) -> bool:
    """
    Download a file from url and save to path

    Args:
        url (str): Url of the file to be downloaded
        path (Path): Path to store the file too
    Returns:
        bool: True if downloaded, False else
    """

    async with aiohttp.ClientSession(
        headers={"User-Agent": user_agent}
    ) as session:
        async with session.get(url) as resp:
            if resp.status == 200:
                async with aiofiles.open(path, mode="wb") as f:
                    await f.write(await resp.read())
                return True
            else:
                return False


def delete_file(path: Path) -> None:
    """
    Delete file if exists

    Args:
       path (Path): The path of the file to delete
    """

    if path.exists():
        path.unlink()


def format_date(timestamp):
    """
    Convert a UNIX timestamp to a date representation. From gpodder
    Obsolete: Will be rendered on qml side
    """

    if timestamp is None:
        return None

    seconds_in_a_day = 60 * 60 * 24

    # try:
    #     timestamp_date = time.localtime(timestamp)[:3]
    # except ValueError as ve:
    #     return None

    try:
        diff = int((time.time() - timestamp) / seconds_in_a_day)
    except:
        return None

    try:
        timestamp = datetime.datetime.fromtimestamp(timestamp)
    except:
        return None

    if diff < 7:
        return timestamp.strftime("%A")
    else:
        return timestamp.strftime("%x")


def format_full_date(timestamp):
    """
    Get a full date string (monday..., may 2018, 2017, ...)
    Obsolete: Will be rendered on qml side
    """

    now = datetime.datetime.now()
    tnow = time.time()
    diff = tnow - timestamp
    if diff < 60 * 60 * 24 * 7:
        return format_date(timestamp)

    dt = datetime.datetime.fromtimestamp(timestamp)
    if dt.year == now.year:
        return dt.strftime("%B %Y")
    return dt.strftime("%Y")


def encrypt(plaintext: str, password: str):
    key = hashlib.blake2s(password.encode()).hexdigest()[:32]
    nonce = get_random_bytes(12)
    cipher = ChaCha20.new(key=key.encode(), nonce=nonce)
    ciphertext = cipher.encrypt(plaintext.encode())
    return (nonce, b64encode(ciphertext))


def decrypt(nonce, ciphertext, password: str):
    key = hashlib.blake2s(password.encode()).hexdigest()[:32]
    ct = b64decode(ciphertext)
    cipher = ChaCha20.new(key=key.encode(), nonce=nonce)
    plaintext = cipher.decrypt(ct)
    return plaintext
