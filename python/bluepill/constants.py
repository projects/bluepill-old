import os
import logging
from pathlib import Path
from peewee import SqliteDatabase, DatabaseProxy, Model

from bluepill import singleton

logger = logging.getLogger(__name__)

db = DatabaseProxy()

class Constants(metaclass=singleton.Singleton):
    """
    manages constants
    """

    def __init__(self, progname="harbour-bluepill"):
        self.progname = progname
        self.__init(progname)

    def __init(self, progname):
        home = Path(os.environ.get("HOME"))

        xdg_data_home = os.environ.get(
            "XDG_DATA_HOME", home / '.local' / 'share'
        )
        xdg_config_home = os.environ.get(
            "XDG_CONFIG_HOME", home / '.config'
        )
        xdg_cache_home = os.environ.get(
            "XDG_CACHE_HOME", home / '.cache'
        )

        if "BLUEPILL_HOME" in os.environ:
            self.data_home = self.config_home = self.cache_home = os.environ["PODQAST_HOME"]
        else:
            self.data_home = xdg_data_home / progname
            self.config_home = xdg_config_home /progname
            self.cache_home = xdg_cache_home / progname

        self.sqlitepath = self.data_home / 'bluepill.sqlite'
        db.initialize(SqliteDatabase(self.sqlitepath))

class BaseModel(Model):
    class Meta:
        database = db
