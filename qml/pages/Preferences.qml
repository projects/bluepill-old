import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page
    allowedOrientations: Orientation.All

    SilicaFlickable {
        id: loginflick
        anchors.fill: parent
        contentHeight: loginTitle.height + loginTitle.spacing + parameters.height + parameters.spacing

        Connections {
            target: clienthandler
            onLoggedOut: {
                pageStack.replace(Qt.resolvedUrl("LoginPage.qml"))
            }
        }

        Column {
            id: loginTitle

            width: page.width
            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("Preferences")
            }
        }


        Column {
            id: parameters
            width: parent.width
            height: children.height
            anchors {
                top: loginTitle.bottom
                left: parent.left
                right: parent.right
                margins: Theme.paddingMedium
            }


            Label {
                width: parent.width
                height: Theme.itemSizeSmall
                text: qsTr('User settings')
                color: Theme.highlightColor
            }

            Label {
                width: parent.width
                height: Theme.itemSizeSmall
                text: qsTr('Security')
                color: Theme.highlightColor
            }

            Item {
                width: parent.width
                height: Theme.itemSizeSmall
                Label {
                   id: sesslabel
                   text: qsTr('My Sessions')
                   anchors.left: parent.left
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: pageStack.push(Qt.resolvedUrl("UserSessions.qml"),
                                              { userid: bluepill.myuser_id })
                }
            }
            Label {
                width: parent.width
                height: Theme.itemSizeSmall
                text: qsTr('Messages')
                color: Theme.highlightColor
            }

            Slider {
                id: fontSize
                width: parent.width
                minimumValue: 1.0
                maximumValue: 2.0
                stepSize: 0.1
                valueText: value
                label: qsTr("Font size")
                onValueChanged: {
                    fontSizeConf.value = fontSize.value
                }
            }

            TextSwitch {
                id: markdownValue
                text: qsTr("Markdown rendering")
                description: qsTr("Render messages with Markdown")
            }

            TextSwitch {
                id: favNotiValue
                text: qsTr("Notify favourites only")
                description: qsTr("Only notify for new messages from favourite rooms")
                onCheckedChanged: {
                    favNotiValueConf.value = favNotiValue.checked
                }
            }
            Button {
                id: logoutButton
                property bool logging: false
                anchors.margins: Theme.paddingLarge
                anchors.bottomMargin: Theme.paddingMedium
                text: qsTr("Logout")
                BusyIndicator {
                    size: BusyIndicatorSize.Medium
                    anchors.centerIn: logoutButton
                    running: logoutButton.logging
                }

                onClicked: {
                    Remorse.popupAction(page, qsTr("Logging out"), function() {
                        logging = true
                        clienthandler.doLogout()
                    })
                }
            }
        }
    }
    Component.onCompleted: {
        favNotiValue.checked = favNotiValueConf.value
        fontSize.value = fontSizeConf.value
    }
}
