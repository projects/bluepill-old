import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page

    property bool isloading: true
    property bool isroom: true

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All


    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaListView {
        anchors.fill: parent
        width: parent.width
        height: parent.height - panel.height

        bottomMargin: panel.margin

        model: ListModel {
            id: roomsModel
        }
        delegate: RoomListItem { }

        PullDownMenu {
            id: pulldown

            MenuItem {
                text: qsTr('Enter room')
                onClicked: pageStack.push(Qt.resolvedUrl("JoinRoom.qml"))
            }
            MenuItem {
                text: qsTr('Create room')
            }
            MenuItem {
                text: qsTr('StartChat')
            }
            MenuItem {
                text: qsTr("Preferences")
                onClicked: pageStack.push(Qt.resolvedUrl("Preferences.qml"))
            }
        }

        Component.onCompleted: {
            clienthandler.getRoomList()
        }

        Connections {
            target: clienthandler
            onRoomList: {
                isloading = false
                roomsModel.clear()
                console.log(data.length, "length")
                for (var i = 0; i < data.length; i++) {
                    if (data[i].is_group === isroom) {
                        roomsModel.append(data[i])
                    }
                }
            }
            onRoomLeaveResponse: {
                isloading = false
                roomsModel.clear()
                console.log(data.length, "length")
                for (var i = 0; i < data.length; i++) {
                    if (data[i].is_group === isroom) {
                        roomsModel.append(data[i])
                    }
                }
            }
            onNewMessage: {
                clienthandler.getRoomList()
            }
        }

        // Tell SilicaFlickable the height of its content.
        contentHeight: column.height + column.spacing + roomslist.height + roomslist.spacing

        // Place our content in a Column.  The PageHeader is always placed at the top
        // of the page, followed by our content.

        BusyIndicator {
            size: BusyIndicatorSize.Large
            anchors.centerIn: parent
            running: isloading
        }

        header: PageHeader {
                    id: pagetitle
                    title: isroom ? qsTr("Direct Messages") : qsTr("Rooms")
                }

        ListView {
                id: roomslist
                width: parent.width
                height: parent.height - pagetitle.height - panel.height
                anchors.margins: Theme.paddingMedium
                // orientation: ListView.HorizontalAndVerticalFlick
        }
    }
    DockedPanel {
        id: panel

        width: parent.width
        height: Theme.itemSizeSmall + Theme.paddingSmall
        open: !pulldown.active
        dock: Dock.Bottom

        Row {
            anchors.centerIn: parent
            IconButton {
                icon.source: "image://theme/icon-s-chat"
                onClicked: {
                    isroom = true
                    clienthandler.getRoomList()
                }
            }
            IconButton {
                icon.source: "image://theme/icon-s-group-chat"
                onClicked: {
                    isroom = false
                    clienthandler.getRoomList()
                }
            }
        }
    }
}
