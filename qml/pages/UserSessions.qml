import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Notifications 1.0

import "../components"

Page {
    id: page
    property string userid
    property string theuserid: userid

    allowedOrientations: Orientation.All

    Component.onCompleted: {
        clienthandler.getUserProfile(userid)
        clienthandler.getActiveUserDevices(userid)
    }


    SilicaListView {
        anchors.fill: parent
        anchors.margins: Theme.paddingMedium
        width: parent.width
        height: parent.height
        contentHeight: loginTitle.contentHeight + devicelist.contentHeight

        model: ListModel {
            id: deviceModel
        }
        delegate: DeviceListItem { }

        Notification {
            id: logFailNotification
            category:"com.gitlab.cy8aer.bluepill"
            appIcon:"image://theme/icon-lock-chat"
            appName:"bluepill"
            urgency: Notification.Critical
        }

        Connections {
            target: clienthandler
            onUserProfile: {
                theuserid = userdata.displayname
            }
            onActiveUserDevices: {
                deviceModel.clear()
                console.log(devices.length, "length")
                for (var i = 0; i < devices.length; i++) {
                    deviceModel.append(devices[i])
                }
            }
            onKeyVerificationMac: {
                clienthandler.getActiveUserDevices(userid)
            }
            onDevicesDeleted: {
                clienthandler.getActiveUserDevices(userid)
            }
            onDevicesDeletionAuthFailed: {
                logFailNotification.previewSummary = qsTr("Login error")
                logFailNotification.previewBody = qsTr("Wrong password")
                logFailNotification.publish()
            }
        }

        // content.height: devicelist.height

        header: PageHeader {
                    id: loginTitle
                    title: qsTr("Devices of ") + theuserid
                }

        ListView {
                id: devicelist
                width: parent.width
                height: parent.height - loginTitle.height
                // height: contentHeight
                // anchors.top: loginTitle.bottom
                anchors.margins: Theme.paddingMedium
                // orientation: ListView.HorizontalAndVerticalFlick
        }
    }
}
