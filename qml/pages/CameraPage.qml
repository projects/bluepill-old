/****************************************************************************
**
** Copyright (C) 2017 Open Mobile Platform Ltd.
** Contact: Kirill Chuvilin <k.chuvilin@omprussia.ru>
**
** This file is a part of qml-camera example project.
** See https://gitlab.com/sailfishos-examples for more examples.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

import QtQuick 2.5
import Sailfish.Silica 1.0

// "qt5-qtmultimedia" should be added
// to the section "Requires" of the yaml-file
import QtMultimedia 5.5 // to use Camera and VideoOutput

// "qt5-qtdeclarative-import-sensors" should be added
// to the section "Requires" of the yaml-file
import QtSensors 5.2 // to use OrientationSensor


Page {
    id: camerapage

    signal photoshot(string imagefilename)

    // Rotation for controls according to orientation
    property int controlsRotation: 0

    // To rotate captured media and controls according to the device orientation
    OrientationSensor { id: orientationSensor; active: true }


    Camera {
        id: camera

        // True if primary camera is used
        property bool primary: deviceId === "primary"

        // Camera device orientation:
        // 0 - unknown, 1 - portraint, 2 - portrait inverted, 3 - landscape, 4 - landscape inverted
        readonly property int orientation: orientationSensor.reading ?
                                               orientationSensor.reading.orientation :
                                               0

        // True to capture after focusing complete
        property bool _captureOnFocus: false

        // Captured media rotation andles for device orientations
        readonly property var _rotations: {
            "primary": [270, 270, 90, 180 ,0],
            "secondary": [90, 90, 270, 180, 0]
        }

        imageCapture {
            onImageSaved: {
                console.log("Photo shot and saved to " + path)
                pageStack.previousPage().previewfile = path
                pageStack.previousPage().showpreview = true
                pageStack.pop()
            }
        }


        // Capture picture to the file
        function capture() {
            if (camera.lockStatus === Camera.Searching) { // if the camera is in focusing stare
                captureView._captureOnFocus = true; // capture after focusing
            } else { // if the camera is focused
                _capturePicture(); // capture immediately
            }
        }

        // Capture picture immediately
        function _capturePicture() {
            camera.metaData.date = new Date(); // store the date to the meta information
            imageCapture.captureToLocation(StandardPaths.pictures + "/" + Qt.application.name +".jpg")
        }

        exposure {
            exposureCompensation: -1.0
            exposureMode: Camera.ExposurePortrait
        }
        imageProcessing.whiteBalanceMode: CameraImageProcessing.WhiteBalanceFlash
        flash.mode: Camera.FlashRedEyeReduction
        captureMode: Camera.CaptureStillImage
        metaData.orientation: _rotations[deviceId][orientation]
        viewfinder {
            minimumFrameRate: 30
            maximumFrameRate: 30
        }
        onPrimaryChanged: { // on camera device changed
            stop(); // stop the current running camera device
            deviceId = primary ? "primary" : "secondary";
            start(); // start the new selected camera device
        }
        onLockStatusChanged: {
            if (lockStatus != Camera.Searching && _captureOnFocus) { // if ready to capture
                _captureOnFocus = false; // don't capture on next focusing
                _capturePicture(); // capture immediately
            }
        }
    }

    // Background to show while device or mode switching
    Rectangle { anchors.fill: parent; color: "black" }

    VideoOutput { // viewfinder
        // Mirror output for secondary camera
        property bool mirror: !camera.primary

        source: camera // show video stream from the camera device
        anchors.fill: parent

        // Touch viewfinder to start focusing
        MouseArea { anchors.fill: parent; onPressed: camera.searchAndLock() }
    }

    Rectangle {
        id: captureButton
        anchors {
            bottom: parent.bottom
            bottomMargin: Theme.paddingLarge
            horizontalCenter: parent.horizontalCenter
        }
        width: Theme.itemSizeLarge
        height: width
        radius: width / 2
        color: captureButtonMouseArea.pressed ? Theme.highlightColor : Theme.primaryColor

        MouseArea {
            id: captureButtonMouseArea
            anchors.fill: parent
            onReleased: if (containsMouse) camera.capture() // capture if release at button
            onPressAndHold: camera.searchAndLock() // start focusing if holding
        }
    }

    IconButton {
        icon.source: "image://theme/icon-m-sync"
        anchors {
            left: parent.left
            leftMargin: Theme.paddingLarge
            verticalCenter: captureButton.verticalCenter
        }
        visible: QtMultimedia.availableCameras.length > 1
        rotation: camerapage.controlsRotation
        onClicked: camera.primary = !camera.primary // switch device
    }

    // Animations to rotate controls according to the orientation
    NumberAnimation on controlsRotation { running: camera.orientation === 1; to: 0;   duration: 200 }
    NumberAnimation on controlsRotation { running: camera.orientation === 2; to: 180; duration: 200 }
    NumberAnimation on controlsRotation { running: camera.orientation === 3; to: -90; duration: 200 }
    NumberAnimation on controlsRotation { running: camera.orientation === 4; to: 90;  duration: 200 }
}
