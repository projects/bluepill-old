import QtQuick 2.2
import Sailfish.Silica 1.0

Dialog {
    id: thedialog

    property string user_id
    property int timeoutval: 30

    Component.onCompleted: {
        clienthandler.getUserProfile(user_id)
    }

    onTimeoutvalChanged: {
        if (timeoutval == 0) {
            thedialog.reject()
        }
    }

    Connections {
        target: clienthandler
        onUserProfile: {
            theusername.text = userdata.displayname
        }
    }

    Column {
        width: parent.width
        anchors.margins: Theme.paddingMedium

        DialogHeader { title: qsTr('Accept Verification?')}

        Item {
              Timer {
                  interval: 1000; running: true; repeat: true
                  onTriggered: timeoutval -= 1
              }
          }

        ProgressBar {
            id: thetimeout
            width: parent.width
            height: Theme.itemSizeSmall
            minimumValue: 0
            maximumValue: 30
            value: timeoutval
        }

        Label {
            width: parent.width
            height: contentHeight
            wrapMode: Text.WordWrap
            padding: Theme.paddingMedium
            id: theusername
            text: user_id
        }

        Label {
            width: parent.width
            height: contentHeight
            padding: Theme.paddingMedium
            wrapMode: Text.WordWrap
            text: qsTr("wants to verify this device.")
        }
    }

//    onDone: {
//        if (result == DialogResult.Accepted) {
//            name = nameField.text
//        }
//    }
}
