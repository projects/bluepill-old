import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4

Python {
    id: clienthandler

    signal loginError(string whyfailed)
    signal serverConnected()
    signal needLogin()
    signal loggedIn(string userid)
    signal initReady(string userid, string deviceid)
    signal loggedOut()
    signal typingNotice(string roomId, string userIds)
    signal syncingRooms(string roomname)
    signal connectFailed()
    signal isConnected()
    signal hostName(string hostname)
    signal roomList(var data)
    signal roomData(var room)
    signal roomInfo(var users)
    signal roomEvents(var events)
    signal roomMembers(string room_id, var members)
    signal roomTagChanged(string room_id, string prio)
    signal userInfo(var userdata)
    signal userProfile(var userdata)
    signal activeUserDevices(var devices)
    signal devicesDeleted(var devices)
    signal roomLeaveResponse(string roomid)
    signal roomMessageText(var room, string ename, var event, bool verified, bool encrypted)
    signal mRedactEvent(string roomId, var redactEvent)
    signal stopTyping(string roomId)
    signal startTyping(string roomId, var users)
    signal messageSent()
    signal imageSendFailed(string errmsg)
    signal imageSent()
    signal restartListener()
    signal roomUnread(string roomId, int count)
    signal bucketGet(string room_id, string bucket_id, var bucket)
    signal bucketGetFrom(string room_id, string bucket_id, var bucket)
    signal joinRoomFailed(string error)
    signal newMessage()
    signal keyVerificationStart(var keydata)
    signal keyVerificationKey(
	string client_user_id,
        string device_user_id,
        string device_id,
        string transaction_id,
        var emojis
    )
    signal keyVerificationMac(
	string client_userid,
        string device_userid,
        string device_id,
        string transaction_id
    )
    signal devicesDeletionAuthFailed(string device_id)

    Component.onCompleted: {
        setHandler("loginError", loginError)
        setHandler("syncingRooms", syncingRooms)
        setHandler("serverConnected", serverConnected)
        setHandler("needLogin", needLogin)
        setHandler("loggedIn", loggedIn)
        setHandler("typingNotice", typingNotice)
        setHandler("connectFailed", connectFailed)
        setHandler("isConnected", isConnected)
        setHandler("initReady", initReady)
        setHandler("hostName", hostName)
        setHandler("roomList", roomList)
        setHandler("roomData", roomData)
        setHandler("roomInfo", roomInfo)
        setHandler("roomEvents", roomEvents)
        setHandler("bucketGet", bucketGet)
        setHandler("bucketGetFrom", bucketGetFrom)
        setHandler("roomMembers", roomMembers)
        setHandler("roomTagChanged", roomTagChanged)
        setHandler("userInfo", userInfo)
        setHandler("userProfile", userProfile)
        setHandler("activeUserDevices", activeUserDevices)
        setHandler("roomMessageText", roomMessageText)
        setHandler("mRedactEvent", mRedactEvent)
        setHandler("startTyping", startTyping)
        setHandler("stopTyping", stopTyping)
        setHandler("messageSent", messageSent)
        setHandler("loggedOut", loggedOut)
        setHandler("restartListener", restartListener)
        setHandler("imageSendFailed", imageSendFailed)
        setHandler("imageSent", imageSent)
        setHandler("roomUnread", roomUnread)
        setHandler("joinRoomFailed", joinRoomFailed)
        setHandler("newMessage", newMessage)
        setHandler("keyVerificationStart", keyVerificationStart)
        setHandler("keyVerificationKey", keyVerificationKey)
        setHandler("keyVerificationMac", keyVerificationMac)
        setHandler("devicesDeleted", devicesDeleted)
        setHandler("devicesDeletionAuthFailed", devicesDeletionAuthFailed)

        addImportPath(Qt.resolvedUrl('.'));
        importModule('ClientHandler', function () {
            console.log('ClientHandler is now imported')
        })
        initSystem()
        bluepill.engineLoaded = true
    }

    function initSystem() {
        console.log('system_init')
        call(
	    "ClientHandler.do_init",
	    [
		StandardPaths.cache,
		StandardPaths.genericData + "/harbour-bluepill"
	    ],
	    function() {})
    }

    function getHostname() {
        // call("SystemHandler.systemhandler.gethostname", function() {})
        call("ClientHandler.get_hostname", function() {})
    }

    function doLogin(user, password, hostname, homeserver) {
        // call("ClientHandler.clienthandler.dologin", [user, password, hostname, homeserver, identserver], function() {})
        call(
	    "ClientHandler.do_login",
	    [
		user,
		password,
		hostname,
		homeserver
	    ],
	    function() {}
	)
    }
    function getRoomList() {
        // call("ClientHandler.clienthandler.get_room_list", function() {})
        call("ClientHandler.get_room_list", function() {})
    }

    function getBucket(room_id, bucket_id) {
        // call("ClientHandler.clienthandler.getbucket, [room_id, bucket_id], function() {})
        call("ClientHandler.get_bucket", [room_id, bucket_id], function() {})
    }

    function getBucketFrom(room_id, bucket_id) {
        // call("ClientHandler.clienthandler.getbucketfrom, [room_id, bucket_id], function() {})
        call("ClientHandler.get_bucket_from", [room_id, bucket_id], function() {})
    }

//    function getRoom(room_id) {
//        // call("ClientHandler.clienthandler.getroom", [room_id], function() {})
//        call("ClientHandler.get_room", [room_id], function() {})
//    }

    function getRoomInfo(room_id) {
        // call("ClientHandler.clienthandler.getinfo", [room_id], function() {})
        call("ClientHandler.get_room_info", [room_id], function() {})
    }

    function getRoomEvents(room_id) {
        // call("ClientHandler.clienthandler.getroomevents", [room_id], function() {})
        call("ClientHandler.get_room_events", [room_id], function() {})
    }

    function joinRoom(room_id_or_alias) {
        // call("ClientHandler.clienthandler.joinroom", [room_id_or_alias], function() {})
        call("ClientHandler.join_room", [room_id_or_alias], function() {})
    }

    function roomLeave(room_id) {
        // call("ClientHandler.clienthandler.roomleave", [room_id], function() {})
        call("ClientHandler.room_leave", [room_id], function() {})
    }

    function getRoomMembers(room_id) {
        call("ClientHandler.clienthandler.getroommembers", [room_id], function() {})
        // call("ClientHandler.get_room_members", [room_id], function() {})
    }

    function getUserInfo() {
        // call("ClientHandler.clienthandler.getuserinfo", function() {})
        call("ClientHandler.get_user_info", function() {})
    }

    function getUserProfile(user_id) {
        // call("ClientHandler.clienthandler.getuserprofile", [user_id], function() {})
        call("ClientHandler.get_user_profile", [user_id], function() {})
    }

    function getActiveUserDevices(user_id) {
        // call("ClientHandler.clienthandler.activeuserdevices", [user_id], function() {})
        call("ClientHandler.active_user_devices", [user_id], function() {})
    }

    function deleteDevices(devices, user_id, password) {
        // call("ClientHandler.clienthandler.deletedevices", [devices, user_id, password], function() {})
        call("ClientHandler.delete_devices", [devices, user_id, password], function() {})
    }

    function sendMessage(room_id, message) {
        // call("ClientHandler.clienthandler.sendmessage", [room_id, message], function() {})
        call("ClientHandler.send_message", [room_id, message], function() {})
    }

    function sendImage(room_id, image_file) {
        // call("ClientHandler.clienthandler.sendimage", [room_id, image_file], function() {})
        call("ClientHandler.send_image", [room_id, image_file], function() {})
    }

    function redactMessage(room_id, event_id, reason) {
        // call("ClientHandler.clienthandler.redactmessage", [room_id, event_id, reason], function() {})
        call("ClientHandler.redact_message", [room_id, event_id, reason], function() {})
    }

    function doLogout() {
        // call("ClientHandler.clienthandler.dologout", function() {})
        call("ClientHandler.do_logout", function() {})
    }

    function seenMessage(room_id) {
        // call("ClientHandler.clienthandler.sendmessage", [room_id, message], function() {})
        call("ClientHandler.seen_message", [room_id], function() {})
    }

    function getUnreadCount(room_id) {
        // call("ClientHandler.clienthandler.getunreadcount", [room_id, message], function() {})
        call("ClientHandler.get_unread_count", [room_id], function() {})
    }

    function isTyping(room_id, timeout) {
        // call("ClientHandler.clienthandler.istyping", [room_id, timeout], function() {})
        call("ClientHandler.is_typing", [room_id, timeout], function() {})
    }

    function restartTheListener() {
        // call("ClientHandler.clienthandler.restartlistener", function() {})
        call("ClientHandler.restart_listener", function() {})
    }

    function startKeyVerification(device_id) {
        // call("ClientHandler.clienthandler.startkeyverification, [device_id], function() {})
        call("ClientHandler.start_key_verification", [device_id], function() {})
    }

    function cancelKeyVerification(transaction_id) {
        // call("ClientHandler.clienthandler.cancelkeyverification, [transaction_id], function() {})
        call("ClientHandler.cancel_key_verification", [transaction_id], function() {})
    }

    function acceptKeyVerification(transaction_id) {
        // call("ClientHandler.clienthandler.acceptkeyverification, [transaction_id], function() {})
        call("ClientHandler.accept_key_verification", [transaction_id], function() {})
    }

    function confirmKeyVerification(transaction_id) {
        // call("ClientHandler.clienthandler.confirmkeyverification, [transaction_id], function() {})
        call("ClientHandler.confirm_key_verification", [transaction_id], function() {})
    }

    function verifyDevice(device_id, user_id) {
        // call("ClientHandler.clienthandler.verifydevice, [device_id, user_id], function() {})
        call("ClientHandler.verify_device", [device_id, user_id], function() {})
    }

    function deleteDevice(device_id, user_id, password) {
        // call("ClientHandler.clienthandler.deletedevice, [device_id, user_id, password], function() {})
        call("ClientHandler.delete_device", [device_id, user_id, password], function() {})
    }

    onError: {
        console.log('python error: ' + traceback);
    }

    onReceived: {
        console.log('got message from python: ' + data);
    }
}
