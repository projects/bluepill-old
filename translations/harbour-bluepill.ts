<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="21"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="32"/>
        <source>[sticker]</source>
        <translation>[sticker]</translation>
    </message>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="34"/>
        <source>[encrypted message]</source>
        <translation>[encrypted message]</translation>
    </message>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="36"/>
        <source>[message redacted]</source>
        <translation>[message redacted]</translation>
    </message>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="38"/>
        <source>[unknown]</source>
        <translation>[unknown]</translation>
    </message>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="113"/>
        <source>is typing...</source>
        <translation>is typing...</translation>
    </message>
</context>
<context>
    <name>DashboardPage</name>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="36"/>
        <source>Create room</source>
        <translation>Create room</translation>
    </message>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="32"/>
        <source>Enter room</source>
        <translation>Enter room</translation>
    </message>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="39"/>
        <source>StartChat</source>
        <translation>Start Chat</translation>
    </message>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="42"/>
        <source>Preferences</source>
        <translation>Preferences</translation>
    </message>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="92"/>
        <source>Direct Messages</source>
        <translation>Direct Messages</translation>
    </message>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="92"/>
        <source>Rooms</source>
        <translation>Rooms</translation>
    </message>
</context>
<context>
    <name>DeleteDevice</name>
    <message>
        <location filename="../qml/pages/DeleteDevice.qml" line="27"/>
        <source>Delete device</source>
        <translation>Delete device</translation>
    </message>
    <message>
        <location filename="../qml/pages/DeleteDevice.qml" line="30"/>
        <source>Device ID</source>
        <translation>Device ID</translation>
    </message>
    <message>
        <location filename="../qml/pages/DeleteDevice.qml" line="45"/>
        <source>Device Name</source>
        <translation>Device Name</translation>
    </message>
    <message>
        <location filename="../qml/pages/DeleteDevice.qml" line="60"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
</context>
<context>
    <name>DeviceListItem</name>
    <message>
        <location filename="../qml/components/DeviceListItem.qml" line="17"/>
        <source>Verify with emojis</source>
        <translation>Verify with emojis</translation>
    </message>
    <message>
        <location filename="../qml/components/DeviceListItem.qml" line="27"/>
        <source>Text verify</source>
        <translation>Text verify</translation>
    </message>
    <message>
        <location filename="../qml/components/DeviceListItem.qml" line="54"/>
        <source>Delete device</source>
        <translation>Delete device</translation>
    </message>
</context>
<context>
    <name>EventsListItem</name>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="31"/>
        <source>copy</source>
        <translation>copy</translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="37"/>
        <source>event source</source>
        <translation>event source</translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="45"/>
        <source>cite</source>
        <translation>cite</translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="53"/>
        <source>delete</source>
        <translation>delete</translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="185"/>
        <source>End to end encryption not implemented</source>
        <translation>End to end encryption is not implemented</translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="187"/>
        <source>redacted</source>
        <translation>redacted</translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="190"/>
        <source>left the room</source>
        <translation>left the room</translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="194"/>
        <source>is now </source>
        <translation>is now</translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="198"/>
        <source>entered the room</source>
        <translation>entered the room</translation>
    </message>
</context>
<context>
    <name>JoinRoom</name>
    <message>
        <location filename="../qml/pages/JoinRoom.qml" line="29"/>
        <source>Join Room</source>
        <translation>Join room</translation>
    </message>
</context>
<context>
    <name>KeyVerificationKey</name>
    <message>
        <location filename="../qml/pages/KeyVerificationKey.qml" line="27"/>
        <source>Compare Icons</source>
        <translation>Compare Icons</translation>
    </message>
</context>
<context>
    <name>KeyVerificationStart</name>
    <message>
        <location filename="../qml/pages/KeyVerificationStart.qml" line="31"/>
        <source>Accept Verification?</source>
        <translation>Accept verification?</translation>
    </message>
    <message>
        <location filename="../qml/pages/KeyVerificationStart.qml" line="63"/>
        <source>wants to verify this device.</source>
        <translation>wants to verify this device</translation>
    </message>
</context>
<context>
    <name>LogStatusPage</name>
    <message>
        <location filename="../qml/pages/LogStatusPage.qml" line="39"/>
        <source>First sync. This may take some time...</source>
        <translation>First sync. This may take some time...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="35"/>
        <source>Login error</source>
        <translation>Login error</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="51"/>
        <location filename="../qml/pages/LoginPage.qml" line="122"/>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="73"/>
        <source>Username</source>
        <translation>Username</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="90"/>
        <source>Client Name</source>
        <translation>Client name</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="97"/>
        <source>Custom Server</source>
        <translation>Custom server</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="102"/>
        <source>Home server URL</source>
        <translation>Home server URL</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="112"/>
        <source>https://matrix.org</source>
        <translation>https://matrix.org</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="154"/>
        <source>Register</source>
        <translation>Register</translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="26"/>
        <source>Preferences</source>
        <translation>Preferences</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="46"/>
        <source>User settings</source>
        <translation>User settings</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="53"/>
        <source>Security</source>
        <translation>Security</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="62"/>
        <source>My Sessions</source>
        <translation>My sessions</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="74"/>
        <source>Messages</source>
        <translation>Messages</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="85"/>
        <source>Font size</source>
        <translation>Font size</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="93"/>
        <source>Markdown rendering</source>
        <translation>Markdown rendering</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="94"/>
        <source>Render messages with Markdown</source>
        <translation>Render messages with Markdown</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="99"/>
        <source>Notify favourites only</source>
        <translation>Notify favourites only</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="100"/>
        <source>Only notify for new messages from favourite rooms</source>
        <translation>Only notify for new messages from favourite rooms</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="110"/>
        <source>Logout</source>
        <translation>Logout</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="118"/>
        <source>Logging out</source>
        <translation>Logging out</translation>
    </message>
</context>
<context>
    <name>RedactMessage</name>
    <message>
        <location filename="../qml/pages/RedactMessage.qml" line="17"/>
        <source>Reason for redaction</source>
        <translation>Reason for redaction</translation>
    </message>
    <message>
        <location filename="../qml/pages/RedactMessage.qml" line="18"/>
        <source>Reason</source>
        <translation>Reason</translation>
    </message>
    <message>
        <location filename="../qml/pages/RedactMessage.qml" line="24"/>
        <source>Do you really wish to redact (delete) this event? This cannot be undone.</source>
        <translation>Do you really wish to redact (delete) this event? This cannot be undone.</translation>
    </message>
</context>
<context>
    <name>RegisterPage</name>
    <message>
        <location filename="../qml/pages/RegisterPage.qml" line="21"/>
        <location filename="../qml/pages/RegisterPage.qml" line="48"/>
        <source>Register</source>
        <translation>Register</translation>
    </message>
    <message>
        <location filename="../qml/pages/RegisterPage.qml" line="42"/>
        <source>Email address (optional)</source>
        <translation>Email address (optional)</translation>
    </message>
</context>
<context>
    <name>RoomListItem</name>
    <message>
        <location filename="../qml/components/RoomListItem.qml" line="28"/>
        <source>Favourite</source>
        <translation>Favourite</translation>
    </message>
    <message>
        <location filename="../qml/components/RoomListItem.qml" line="33"/>
        <source>Low priority</source>
        <translation>Low priority</translation>
    </message>
    <message>
        <location filename="../qml/components/RoomListItem.qml" line="40"/>
        <source>Leave</source>
        <translation>Leave</translation>
    </message>
    <message>
        <location filename="../qml/components/RoomListItem.qml" line="42"/>
        <source>leaving room</source>
        <translation>leaving room</translation>
    </message>
    <message>
        <location filename="../qml/components/RoomListItem.qml" line="48"/>
        <source>Preferences</source>
        <translation>Preferences</translation>
    </message>
</context>
<context>
    <name>RoomPage</name>
    <message>
        <location filename="../qml/pages/RoomPage.qml" line="79"/>
        <source>Preferences: </source>
        <translation>Preferences: </translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomPage.qml" line="83"/>
        <source>Preferences</source>
        <translation>Preferences</translation>
    </message>
</context>
<context>
    <name>StartPage</name>
    <message>
        <location filename="../qml/pages/StartPage.qml" line="13"/>
        <source>Connected...</source>
        <translation>Connected...</translation>
    </message>
    <message>
        <location filename="../qml/pages/StartPage.qml" line="16"/>
        <source>Not connected, trying again later...</source>
        <translation>Not connected, trying again later...</translation>
    </message>
    <message>
        <location filename="../qml/pages/StartPage.qml" line="44"/>
        <source>Starting engine...</source>
        <translation>Starting engine...</translation>
    </message>
</context>
<context>
    <name>TextControl</name>
    <message>
        <location filename="../qml/components/TextControl.qml" line="57"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/components/TextControl.qml" line="112"/>
        <source> is typing...</source>
        <translation>is typing...</translation>
    </message>
</context>
<context>
    <name>TextVerification</name>
    <message>
        <location filename="../qml/pages/TextVerification.qml" line="27"/>
        <source>Verify device</source>
        <translation>Verify device</translation>
    </message>
    <message>
        <location filename="../qml/pages/TextVerification.qml" line="46"/>
        <source>Device ID</source>
        <translation>Device ID</translation>
    </message>
    <message>
        <location filename="../qml/pages/TextVerification.qml" line="61"/>
        <source>Device Name</source>
        <translation>Device name</translation>
    </message>
    <message>
        <location filename="../qml/pages/TextVerification.qml" line="76"/>
        <source>User ID</source>
        <translation>User ID</translation>
    </message>
    <message>
        <location filename="../qml/pages/TextVerification.qml" line="91"/>
        <source>Session Key</source>
        <translation>Session Key</translation>
    </message>
</context>
<context>
    <name>UploadSelector</name>
    <message>
        <location filename="../qml/pages/UploadSelector.qml" line="17"/>
        <source>Upload...</source>
        <translation>Upload...</translation>
    </message>
    <message>
        <location filename="../qml/pages/UploadSelector.qml" line="21"/>
        <source>Image</source>
        <translation>Image</translation>
    </message>
    <message>
        <location filename="../qml/pages/UploadSelector.qml" line="25"/>
        <source>Take a picture</source>
        <translation>Take a picture</translation>
    </message>
    <message>
        <location filename="../qml/pages/UploadSelector.qml" line="98"/>
        <source>Send image</source>
        <translation>Send image</translation>
    </message>
</context>
<context>
    <name>UserSessions</name>
    <message>
        <location filename="../qml/pages/UserSessions.qml" line="59"/>
        <source>Login error</source>
        <translation>Login error</translation>
    </message>
    <message>
        <location filename="../qml/pages/UserSessions.qml" line="60"/>
        <source>Wrong password</source>
        <translation>Wrong password</translation>
    </message>
    <message>
        <location filename="../qml/pages/UserSessions.qml" line="69"/>
        <source>Devices of </source>
        <translation>Devices of </translation>
    </message>
</context>
<context>
    <name>harbour-bluepill</name>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="60"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="46"/>
        <source>never</source>
        <translation>never</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="61"/>
        <source>just now</source>
        <translation>just now</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="62"/>
        <source>1 minute ago</source>
        <translation>1 minute ago</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="63"/>
        <source> minutes ago</source>
        <translation> minutes ago</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="64"/>
        <source>1 hour ago</source>
        <translation>1 hour ago</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="65"/>
        <source> hours ago</source>
        <translation> hours ago</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="68"/>
        <source>Yesterday</source>
        <translation>Yesterday</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="69"/>
        <source> days ago</source>
        <translation> days ago</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="70"/>
        <source> weeks ago</source>
        <translation> weeks ago</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="137"/>
        <source>New posts available</source>
        <translation>New posts available</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="138"/>
        <source>Click to view updates</source>
        <translation>Click to view updates</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="139"/>
        <source>New Posts are available. Click to view.</source>
        <translation>New posts available. Click to view.</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="163"/>
        <source>Room login failed</source>
        <translation>Room login failed</translation>
    </message>
</context>
</TS>
